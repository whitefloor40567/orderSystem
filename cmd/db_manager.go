package cmd

import (
	"orderSystem/database"

	"github.com/spf13/cobra"
)

var cmdDBManager = &cobra.Command{
	Use:   "db_manager",
	Short: "This is the main command used to start other database commands",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

var cmdMariaMigrate = &cobra.Command{
	Use:   "maria_migrate",
	Short: "Start migrate maria Database ",
	Run: func(cmd *cobra.Command, args []string) {
		maria := database.MariaDBConnection()

		database.Migrate(maria)
	},
}
