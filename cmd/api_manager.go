package cmd

import (
	"orderSystem/api"
	"orderSystem/database"

	"github.com/spf13/cobra"
)

var cmdAPIServer = &cobra.Command{
	Use:   "api_server",
	Short: "Print the version number of Hugo",
	Long:  `All software has versions. This is Hugo's`,
	Run: func(cmd *cobra.Command, args []string) {
		//gin engine
		gEngine := api.InitialGin()
		//maria client
		database.BuildConnections()

		api.SystemGateway(gEngine)

		gEngine.Run(":8080")
	},
}
