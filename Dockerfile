FROM golang:latest
WORKDIR /orderSystem
ADD . /orderSystem
COPY source dest
RUN cd /orderSystem && go build
ENTRYPOINT ./orderSystem
CMD go run main.go api_server

