package model

// APIUsers ...
type APIUsers struct {
	UserID   string `binding:"required"`
	Password string `binding:"required"`
	Name     string `binding:"required"`
	Email    string `binding:"required"`
}

// APILogin ...
type APILogin struct {
	UserID   string `binding:"required"`
	Password string `binding:"required"`
}

// APIVerifyIdentidy ...
type APIVerifyIdentidy struct {
	UserID string `binding:"required"`
	Cookie string `binding:"required"`
}

// APIRestaurant ...
type APIRestaurant struct {
	Name            string `binding:"required"`
	Address         string
	TelephoneNumber string
}

// APIMenu ...
type APIMenu struct {
	RestaurantID uint    `binding:"required"`
	MealName     string  `binding:"required"`
	Price        float64 `binding:"required"`
}

// APIAddOrder ...
type APIAddOrder struct {
	UserID       uint `binding:"required"`
	RestaurantID uint `binding:"required"`
}

// APIAddOrderHistory ...
type APIAddOrderHistory struct {
	OrdersID uint `binding:"required"`
	MenuID   uint `binding:"required"`
	Orderer  string
	Remarks  string
}

// APICloseOrder ...
type APICloseOrder struct {
	UserID  uint `binding:"required"`
	OrderID uint `binding:"required"`
}
