package model

import "time"

// OrderResult ...
type OrderResult struct {
	CreatedAt time.Time
	Email     string
	Orderer   string
	MealName  string
	Price     uint
	Remarks   string
}
