package model

import (
	"github.com/jinzhu/gorm"
)

// Users ...
type Users struct {
	gorm.Model
	UserID   string `gorm:"type:varchar(15);unique;not null"`
	Password string `gorm:"type:varchar(90);not null"`
	Name     string `gorm:"type:varchar(20);not null"`
	Email    string `gorm:"type:varchar(40);unique;not null"`
	UUID     string `gorm:"type:varchar(36)"`
}

// Restaurant ...
type Restaurant struct {
	gorm.Model
	Name            string `gorm:"type:varchar(30);not null"`
	Address         string `gorm:"type:varchar(80)"`
	TelephoneNumber string `gorm:"type:varchar(15)"`
	Menu            []*Menu
}

// Menu ...
type Menu struct {
	gorm.Model
	RestaurantID uint    `gorm:"type:int(10) unsigned;not null"`
	MealName     string  `gorm:"type:varchar(30);not null"`
	Price        float64 `gorm:"not null"`
}

// Orders ...
type Orders struct {
	gorm.Model
	UserID       uint            `gorm:"type:int(10) unsigned;not null"`
	RestaurantID uint            `gorm:"type:int(10) unsigned;not null"`
	Users        Users           `gorm:"ForeignKey:UserID"`
	Restaurant   Restaurant      `gorm:"ForeignKey:RestaurantID"`
	OrderHistory []*OrderHistory `gorm:"ForeignKey:OrdersID"`
}

// OrderHistory ...
type OrderHistory struct {
	gorm.Model
	OrdersID uint   `gorm:"type:int(10) unsigned;not null"`
	MenuID   uint   `gorm:"type:int(10) unsigned;not null"`
	Orderer  string `gorm:"type:varchar(30)"`
	Remarks  string `gorm:"type:varchar(50)"`
	Menu     Menu   `gorm:"ForeignKey:MenuID"`
}
