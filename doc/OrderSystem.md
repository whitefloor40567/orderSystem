# OrderSystem API 功能簡介

* Greeting：用來測試API有無正常啟動
* RegisteredMember：註冊會員，密碼會加密
* Login：登入系統，並會給予cookie
* Logout：登出系統，並清除cookie
* AddRestaurant：新增餐廳資訊，會驗證cookie成功後才能使用
* AddMenu：新增菜單資訊，會驗證cookie成功後才能使用
* AddOrder：新增訂單，會驗證cookie成功後才能使用
* CloseOrder：結算訂單，並產出excel檔，會驗證cookie成功後才能使用
* AddOrderHistory：新增訂單內容