package middleware

import (
	"net/http"
	"orderSystem/tools"
	"time"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

// NewUUID ...
func NewUUID() string {
	randomUUID := uuid.NewV4()
	return randomUUID.String()
}

// SetLoginCookie ...
func SetLoginCookie(ctx *gin.Context, myUUID string) {
	expiration := time.Now().AddDate(0, 1, 0)

	cookie := http.Cookie{
		Name:    tools.Cookie,
		Value:   myUUID,
		Expires: expiration,
	}

	http.SetCookie(ctx.Writer, &cookie)
}
