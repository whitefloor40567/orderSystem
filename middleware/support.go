package middleware

import (
	"log"
	"net/http"
	"orderSystem/database"
	"orderSystem/tools"

	"github.com/gin-gonic/gin"
)

// APIGreeting ...
func APIGreeting(ctx *gin.Context) {
	greetingStr := "Hi you successfully start the API"

	log.Println(greetingStr)
	ctx.String(http.StatusOK, greetingStr)
}

// VerifyIdentidy ...
func VerifyIdentidy(ctx *gin.Context) {
	cookie, err := ctx.Request.Cookie(tools.Cookie)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	dbErr := database.DBConnectionsInstance.VerifyIdentidyQuery(cookie.Value)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}
}
