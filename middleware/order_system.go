package middleware

import (
	"net/http"
	"orderSystem/database"
	"orderSystem/model"
	"orderSystem/tools"

	"github.com/gin-gonic/gin"
)

// RegisteredMember ...
func RegisteredMember(ctx *gin.Context) {
	var param model.APIUsers

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	dbErr := database.DBConnectionsInstance.RegisteredMemberQuery(&param)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	ctx.JSON(http.StatusOK, "")
}

// Login ...
func Login(ctx *gin.Context) {
	var param model.APILogin

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	myUUID := NewUUID()

	dbErr := database.DBConnectionsInstance.LoginQuery(&param, myUUID)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	SetLoginCookie(ctx, myUUID)

	ctx.JSON(http.StatusOK, "Login success")
}

// Logout ...
func Logout(ctx *gin.Context) {
	cookie, err := ctx.Request.Cookie(tools.Cookie)
	if err != nil {
		return
	}

	cookie.MaxAge = -1

	http.SetCookie(ctx.Writer, cookie)

	ctx.JSON(http.StatusOK, "")
}

// AddRestaurant ...
func AddRestaurant(ctx *gin.Context) {
	var param model.APIRestaurant

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	dbErr := database.DBConnectionsInstance.AddRestaurantQuery(&param)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	ctx.JSON(http.StatusOK, &param)
}

// AddMenu ...
func AddMenu(ctx *gin.Context) {
	var param model.APIMenu

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	dbErr := database.DBConnectionsInstance.AddMenuQuery(&param)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	ctx.JSON(http.StatusOK, &param)
}

// AddOrder ...
func AddOrder(ctx *gin.Context) {
	var param model.APIAddOrder

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	dbErr := database.DBConnectionsInstance.AddOrderQuery(&param)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	ctx.JSON(http.StatusOK, &param)
}

// AddOrderHistory ...
func AddOrderHistory(ctx *gin.Context) {
	var param model.APIAddOrderHistory

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	dbErr := database.DBConnectionsInstance.AddOrderHistoryQuery(&param)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	ctx.JSON(http.StatusOK, &param)
}

// CloseOrder ...
func CloseOrder(ctx *gin.Context) {
	var param model.APICloseOrder

	bindErr := ctx.Bind(&param)
	if bindErr != nil {
		ctx.JSON(http.StatusBadRequest, bindErr)
		return
	}

	validateErr := tools.ValidateStruct(&param)
	if validateErr != nil {
		ctx.JSON(http.StatusBadRequest, validateErr)
		return
	}

	// check user and query date
	order, dbErr := database.DBConnectionsInstance.FindCloseOrderQuery(&param)
	if dbErr != nil {
		ctx.JSON(http.StatusBadRequest, dbErr)
		return
	}

	// write excel
	personalExcelPath := tools.WriteExcelAboutOrder(order)

	// // copy data to gin
	ctx.File(personalExcelPath)

	return
}
