package database

import (
	"log"
	"net/http"
	"orderSystem/model"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// RegisteredMemberQuery ...
func (maria *DBConnections) RegisteredMemberQuery(param *model.APIUsers) error {
	var model model.Users

	pwd, _ := bcrypt.GenerateFromPassword([]byte(param.Password), bcrypt.MinCost)

	if param.UserID != "" && param.Password != "" && param.Name != "" && param.Email != "" {
		model.UserID = param.UserID
		model.Password = string(pwd)
		model.Name = param.Name
		model.Email = param.Email
	}

	gorm := maria.MariaDB.Create(&model)

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// LoginQuery ...
func (maria *DBConnections) LoginQuery(param *model.APILogin, myUUID string) error {
	var model model.Users

	gorm := maria.MariaDB.Where("user_id=?", param.UserID).First(&model)
	if gorm.RowsAffected == 0 {
		return http.ErrBodyNotAllowed
	}

	pwdErr := bcrypt.CompareHashAndPassword([]byte(model.Password), []byte(param.Password))
	if pwdErr != nil {
		return http.ErrBodyNotAllowed
	}

	gorm = maria.MariaDB.Model(&model).Update("uuid", myUUID)

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// VerifyIdentidyQuery ...
func (maria *DBConnections) VerifyIdentidyQuery(myUUID string) error {
	var model model.Users

	gorm := maria.MariaDB.Where("uuid=?", myUUID).First(&model)

	if gorm.RowsAffected == 0 {
		return http.ErrNoCookie
	}

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// AddRestaurantQuery ...
func (maria *DBConnections) AddRestaurantQuery(param *model.APIRestaurant) error {
	var model model.Restaurant

	if param.Name != "" {
		model.Name = param.Name
	}
	if param.Address != "" {
		model.Address = param.Address
	}
	if param.TelephoneNumber != "" {
		model.TelephoneNumber = param.TelephoneNumber
	}

	gorm := maria.MariaDB.Create(&model)

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// AddMenuQuery ...
func (maria *DBConnections) AddMenuQuery(param *model.APIMenu) error {
	var model model.Menu

	if param.RestaurantID > 0 {
		model.RestaurantID = param.RestaurantID
	}
	if param.MealName != "" {
		model.MealName = param.MealName
	}
	if param.Price > 0 {
		model.Price = param.Price
	}

	gorm := maria.MariaDB.Create(&model)

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// AddOrderQuery ...
func (maria *DBConnections) AddOrderQuery(param *model.APIAddOrder) error {
	var model model.Orders

	if param.UserID > 0 {
		model.UserID = param.UserID
	}
	if param.RestaurantID > 0 {
		model.RestaurantID = param.RestaurantID
	}

	gorm := maria.MariaDB.Create(&model)

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// AddOrderHistoryQuery ...
func (maria *DBConnections) AddOrderHistoryQuery(param *model.APIAddOrderHistory) error {
	var model model.OrderHistory

	if param.OrdersID > 0 {
		model.OrdersID = param.OrdersID
	}
	if param.MenuID > 0 {
		model.MenuID = param.MenuID
	}

	model.Orderer = param.Orderer

	if param.Remarks != "" {
		model.Remarks = param.Remarks
	}

	gorm := maria.MariaDB.Create(&model)

	if gorm.Error != nil {
		return gorm.Error
	}

	return nil
}

// FindCloseOrderQuery ...
func (maria *DBConnections) FindCloseOrderQuery(param *model.APICloseOrder) (*[]model.OrderResult, error) {
	var model []model.OrderResult

	gorm := maria.MariaDB.
		Raw(`
	select orders.created_at,users.email,order_history.orderer,menu.meal_name,menu.price,order_history.remarks
	from orders
	left join order_history on orders.id = order_history.orders_id
	left join menu on order_history.menu_id = menu.id
	left join users on orders.user_id = users.id
	where users.id=? and orders.id =?`, param.UserID, param.OrderID).
		Scan(&model)

	if gorm.Error != nil {
		return nil, gorm.Error
	}

	log.Println((model)[0].CreatedAt)

	return &model, nil
}

// RawQuerySearchAndParseToMap ...
func RawQuerySearchAndParseToMap(db *gorm.DB, query string) ([]map[string]interface{}, error) {
	//Use raw query
	rows, err := db.Raw(query).Rows()
	if err != nil {
		log.Println(err)
		return nil, err
	}

	//取得搜尋回來的資料所擁有的column
	columns, er := rows.Columns()
	if er != nil {
		log.Println(er)
		return nil, er
	}
	columnLength := len(columns)

	//make一個臨時儲存的地方，並賦予指標
	cache := make([]interface{}, columnLength)
	for index := range cache {
		var a interface{}
		cache[index] = &a
	}

	var list []map[string]interface{}
	for rows.Next() {
		rows.Scan(cache...)

		item := make(map[string]interface{})
		for i, data := range cache {
			item[columns[i]] = *data.(*interface{}) //column可能有許多種data type，因此在這取出時不指定型別，否則會轉換錯誤，且在這取出時為uint8(btye array)格式
		}

		list = append(list, item)
	}

	//將byte array轉換為字串
	for index := range list {
		for _, column := range columns {
			list[index][column] = list[index][column].(string)
		}
	}

	return list, nil

}
