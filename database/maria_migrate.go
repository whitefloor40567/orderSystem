package database

import (
	"log"
	"orderSystem/model"

	"github.com/jinzhu/gorm"
)

// Migrate ...
func Migrate(db *gorm.DB) {
	log.Println("---Start Migrate---")

	db.LogMode(true)

	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
		&model.Users{},
		&model.Restaurant{},
		&model.Menu{},
		&model.Orders{},
		&model.OrderHistory{},
	)

	db.Model(&model.Menu{}).AddForeignKey("restaurant_id", "restaurant(id)", "RESTRICT", "RESTRICT")
	db.Model(&model.Orders{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
	db.Model(&model.Orders{}).AddForeignKey("restaurant_id", "restaurant(id)", "RESTRICT", "RESTRICT")
	db.Model(&model.OrderHistory{}).AddForeignKey("orders_id", "orders(id)", "RESTRICT", "RESTRICT")
	db.Model(&model.OrderHistory{}).AddForeignKey("menu_id", "menu(id)", "RESTRICT", "RESTRICT")

	if db.Error != nil {
		db.Rollback()
		log.Println(db.Error)
	}

	log.Println("---Finish Migrate---")
}
