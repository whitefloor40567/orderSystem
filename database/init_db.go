package database

import (
	"log"

	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"

	//init mysql
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// DBConnections ...
type DBConnections struct {
	MariaDB *gorm.DB
}

// DBConnectionsInstance ...
var DBConnectionsInstance DBConnections

// BuildConnections ...
func BuildConnections() {
	var Conns DBConnections

	if Conns.MariaDB == nil {
		maria := MariaDBConnection()
		DBConnectionsInstance.MariaDB = maria
	}
}

// MariaDBConnection ...
func MariaDBConnection() *gorm.DB {
	db, err := gorm.Open(viper.GetString("MySQL"), viper.GetString("MySQLConnectString"))
	if err != nil {
		log.Println(err)
		return nil
	}
	db.SingularTable(true)
	log.Printf("Connection to %v\n", viper.GetString("MySQLConnectString"))

	return db
}
