package api

import (
	"github.com/gin-gonic/gin"
)

// SystemGateway ...
func SystemGateway(g *gin.Engine) {
	GreetingService(g)
	OrderSystemService(g)
}
