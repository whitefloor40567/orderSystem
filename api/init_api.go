package api

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// InitialGin ...
func InitialGin() *gin.Engine {
	gin.SetMode(viper.GetString("GinMode"))
	gEngine := gin.Default()

	return gEngine
}
