package api

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/magiconair/properties/assert"
)

// TestGreetingService ...
func TestGreetingService(t *testing.T) {
	// start gin
	gEngine := InitialGin()
	// instance router
	GreetingService(gEngine)

	// new recorder
	writer := httptest.NewRecorder()
	// request
	req, _ := http.NewRequest("GET", "/Greeting", nil)

	// start httpServer
	gEngine.ServeHTTP(writer, req)

	// check result
	assert.Equal(t, writer.Code, http.StatusOK)
	assert.Equal(t, writer.Body.String(), "Hi you successfully start the API")
}
