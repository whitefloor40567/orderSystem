package api

import (
	"orderSystem/middleware"

	"github.com/gin-gonic/gin"
)

// GreetingService ...
func GreetingService(g *gin.Engine) {
	g.Any("/Greeting", middleware.APIGreeting)
}

// OrderSystemService ...
func OrderSystemService(g *gin.Engine) {
	g.POST("/RegisteredMember", middleware.RegisteredMember)
	g.POST("/Login", middleware.Login)
	g.POST("/Logout", middleware.Logout)

	enter := g.Group("/enter", middleware.VerifyIdentidy)
	{
		enter.POST("/AddRestaurant", middleware.AddRestaurant)
		enter.POST("/AddMenu", middleware.AddMenu)

		enter.POST("/AddOrder", middleware.AddOrder)
		enter.POST("/CloseOrder", middleware.CloseOrder)
	}

	enter.POST("/AddOrderHistory", middleware.AddOrderHistory)
}
