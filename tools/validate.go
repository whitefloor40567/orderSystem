package tools

import "gopkg.in/go-playground/validator.v9"

// ValidateStruct ...
func ValidateStruct(param interface{}) error {
	validate := validator.New()
	validateErr := validate.Struct(param)

	return validateErr
}
