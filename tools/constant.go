package tools

const (
	// Open ...
	Open string = "Open"
	// Close ...
	Close string = "Close"
	// ExcelFilePath ...
	ExcelFilePath string = "./ExcelAboutOrder"
	// Sheet1 ...
	Sheet1 string = "Sheet1"
	// Cookie ...
	Cookie string = "OrderSystem"
)
