package tools

import (
	"fmt"
	"log"
	"orderSystem/model"
	"os"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// CheckSaveExcelFilePath ...
func CheckSaveExcelFilePath() error {
	folderErr := os.MkdirAll(ExcelFilePath, os.ModePerm)
	if folderErr != nil {
		log.Println(folderErr)
		return folderErr
	}
	return nil
}

// WriteExcelAboutOrder ...
func WriteExcelAboutOrder(Order *[]model.OrderResult) (Path string) {
	CheckSaveExcelFilePath()
	excel := excelize.NewFile()

	excel.SetSheetRow(Sheet1, "A1", &[]interface{}{"Orderer", "Meals", "Price", "Remarks"})

	for index, orderValue := range *Order {
		excel.SetSheetRow(Sheet1, fmt.Sprintf("A%v", index+2), &[]interface{}{orderValue.Orderer, orderValue.MealName, orderValue.Price, orderValue.Remarks})
	}

	personalExcelPath := ExcelFilePath + fmt.Sprintf("/%v-%v.xlsx", (*Order)[0].Email, (*Order)[0].CreatedAt.Format("2006-01-02 15:04:05"))
	if excelErr := excel.SaveAs(personalExcelPath); excelErr != nil {
		log.Println(excelErr)
	}

	return personalExcelPath
}
