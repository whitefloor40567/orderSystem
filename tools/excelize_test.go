package tools

import "testing"

// TestCheckSaveExcelFilePath ...
func TestCheckSaveExcelFilePath(t *testing.T) {
	err := CheckSaveExcelFilePath()
	if err != nil {
		t.Errorf("CheckSaveExcelFilePath() error = %v", err)
	}
}
